if has('gui_running')
	source ~/.vim/future
else
	source ~/.vim/vanilla
end
